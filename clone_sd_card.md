# Clone SD card of Jetson Nano using Ubuntu
## Referenced by 
* [Clone SD Card – Jetson Nano and Xavier NX](https://www.jetsonhacks.com/2020/08/08/clone-sd-card-jetson-nano-and-xavier-nx/)


## Table of contents
* [Check device name](#check-device-name)
* [Clone the drive](#clone-the-drive)
* [Restore the drive](#restore-the-drive)

## Check device name
* Plug the card into the host computer
* Check for the device name using the command line:
    ```
    $ sudo parted -l
    ```
* You will see an entry similar to:
    ```
    Model: Multiple Card Reader (scsi)                                        
    Disk /dev/sdb: 63.9GB
    Sector size (logical/physical): 512B/512B
    Partition Table: gpt
    Disk Flags: 

    Number  Start   End     Size    File system  Name     Flags
     2      1049kB  1180kB  131kB                TBC
     3      2097kB  2556kB  459kB                RP1
     4      3146kB  3736kB  590kB                EBT
     5      4194kB  4260kB  65.5kB               WB0
     6      5243kB  5439kB  197kB                BPF
     7      6291kB  6685kB  393kB                BPF-DTB
     8      7340kB  7406kB  65.5kB               FX
     9      8389kB  8847kB  459kB                TOS
    10      9437kB  9896kB  459kB                DTB
    11      10.5MB  11.3MB  786kB                LNX
    12      11.5MB  11.6MB  65.5kB               EKS
    13      12.6MB  12.8MB  197kB                BMP
    14      13.6MB  13.8MB  131kB                RP4
     1      14.7MB  63.9GB  63.8GB  ext4         APP
    ```
## Clone the drive
* Make sure that the card is not mounted:
    ```
    $ sudo umount /dev/sdX
    ```
    Replace sdX with the actual device. Then:
    ```
    $ sudo dd if=/dev/sdX conv=sync,noerror bs=64K | gzip -c > ~/backup_image.img.gz
    ```
    Naturally, you can name the image file and place it where you want. The above will place the backup_image.img.gz file in the home directory.

## Restore the drive
* Using a different µSD card that is the same size or larger than the original, insert the new card into the host computer.
    ```
    $ sudo su
    $ gunzip -c ~/backup_image.img.gz | dd of=/dev/sdX bs=64K
    ```
    Once this process is complete, you should go check it out in your Jetson!