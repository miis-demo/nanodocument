# Build OS-image for Jetson Nano
#### Manually install flash software to your host PC without JetPack/SDKM.
## Referenced by 
* [NVIDIA Developer forum](https://forums.developer.nvidia.com/t/how-to-build-os-image-for-jetson-nano/168343/2)


## Table of contents
* [Download L4T Driver Package](#download-l4t-driver-package)
* [Unpack Package](#unpack-package)
* [Setup the rootfs](#setup-the-rootfs)
* [Generate an image to be flashed](#generate-an-image-to-be-flashed)
* [Put the target device into recovery mode](#put-the-target-device-into-recovery-mode)
* [Flash the target device](#flash-the-target-device)


## Download L4T Driver Package
You can find the different L4T releases of the driver package here: https://developer.nvidia.com/linux-tegra

Download L4T driver package
```
wget https://developer.nvidia.com/embedded/l4t/r32_release_v6.1/t210/jetson-210_linux_r32.6.1_aarch64.tbz2
```
Download sample root filesystem
```
wget https://developer.nvidia.com/embedded/l4t/r32_release_v6.1/t210/tegra_linux_sample-root-filesystem_r32.6.1_aarch64.tbz2
```
## Unpack Package
Unpack the driver package into an empty directory as a regular user (which creates a “Linux_for_Tegra/” subdirectory).
```
$ tar -jxvf L4T_Driver_Package.tbz2 -C /PATH
Example :
$ tar -jxvf Jetson-210_Linux_R32.6.1_aarch64.tbz2 -C ~/nvidia/
```

Unpack the sample rootfs into the new “Linux_for_Tegra/rootfs/” directory as root/sudo.
```
$ sudo tar -jxvf RootFS.tbz2 -C ~/PATH/Linux_for_Tegra/rootfs/
Example :
$ sudo tar -jxvf Tegra_Linux_Sample-Root-Filesystem_R32.6.1_aarch64.tbz2 -C ~/nvidia/Linux_for_Tegra/rootfs/
```

## Setup the rootfs
Go back to “Linux_for_Tegra/”
```
$ cd <your_L4T_root>/Linux_for_Tegra
```
Run script to copy the NVIDIA user space libraries into the target file system.
```
$ sudo ./apply_binaries.sh
```
If the apply_binaries.sh script installs the binaries correctly, the last message output from the script is “Success!”.

## Generate an image to be flashed
```
$ cd <your_L4T_root>/Linux_for_Tegra/tools
$ ./jetson-disk-image-creator.sh -o <image_name> -b <board> -r <revision>
Example:
$ sudo ./jetson-disk-image-creator.sh -o sd-blob.img -b jetson-nano -r 200
```
## Put the target device into recovery mode
* Jumper the force recovery pins (3 and 4) on J40 button header
* Jumper the J48 power select pin and plug the power jack
* Remove the jumper of force recovery pin
* Connect taget device and host with micro-usb

You should be able to see nvidia device with “lsusb” command on your host. It should be _"0955:7f21"_.
```
Bus 003 Device 018: ID 0955:7f21 NVidia Corp.
```

## Flash the target device
Run flash.sh to flash device
```
sudo ./flash.sh jetson-nano-qspi-sd mmcblk0p1
```